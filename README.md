# Catering and Hospitality Dataset (CHD) Documentation

Catering and Hospitality Data (CHD) includes information about restaurants that includes annual sales, type of cuisine, geographical location, number of employees, etc. This repository hosts the code that was used to clean the dataset.

### Version 4.0
##### RELEASED 12.19.19
* Changed the groupings of the menus following [Menu Type Grouping Proposal](https://docs.google.com/document/d/18BBmiJ1yGnzcFxUITO22CKcbhlmTrBedTP2wO-VmdgE/edit "Menu Type Grouping Proposal") set by Data Science (Also see Appendix)
* Fixed a bug that caused the average check minimums and maximums to not display the correct values when the average check was under $5
* Uploaded a new version of table in BigQuery to ‘zume-data’
    * Dataset = ‘chd_prod_dev’’
    * Table name = ‘chd_cleaned_v4’

### Version 3.0
##### RELEASED 12.11.19
* Added an aggregated column for menu type that groups together certain cuisines (i.e. Seafood, Barbecue, Fried chicken, etc. get grouped into a ‘meat’ group)
* Added indicator columns where the column name is a cuisine and the value is 1 if that restaurant is grouped under that cuisine and a 0 if it is not
    * Groupings can be seen in the appendix
* Uploaded a new version of table in BigQuery to ‘zume-data’
    * Dataset = ‘chd_prod_dev’’
    * Table name = ‘chd_cleaned_v3’

### Version 2.0
##### RELEASED 11.25.19
* Fixed a bug that put latitude in the longitude column so that data for latitude and longitude was only showing latitude data
* Uploaded a new version of table in BigQuery to ‘zume-data’
    * Dataset = ‘chd_prod_dev’’
    * Table name = ‘chd_cleaned_v2’

### Version 1.0
##### RELEASED 10.24.19
* Uploaded a first version of table in BigQuery to ‘zume-data’
    * Dataset = ‘chd_prod_dev’’
    * Table name = ‘chd_cleaned_v1’
* Kept the following columns [See Appendix]
* Dropped the following columns [See Appendix]
    * Notably dropped PII data
        * Phone
        * First Name
        * Last Name
        * Contact Job Title
* Fixed typecasting (columns that should be numerics are now numerics)
    * Latitude and longitude
    * Food, Beverage, and Disposable Purchase Potential
* Split the following columns into multiple columns (and converted to numerics if needed)
    * **Dayparts** → BREAKFAST, LUNCH, DINNER
        * Type: String
        * ‘YES’ = restaurant serves that part of the day
        * ‘NO’ = restaurant serves that part of the day
        * ‘99’ means no data available
    * **Store hours** → SUN_OPEN, SUN_CLOSE, MON_OPEN, MON_CLOSE, ... ,  SAT OPEN, SAT CLOSE
        * Type: Integer
        * Represents the time that a restaurant opens and closes
        * 24-hour clock from 1 to 24
        * All times follow this mapping:
            * 12:30AM - 01:29AM → 1
            * 01:30AM - 02:29AM → 2
            * …
            * 10:30PM - 11:29PM → 23
            * 11:30PM - 12:29AM → 24
        * If the restaurant is closed on a day, the value will be 0 for both columns
        * If the restaurant is open 24 hours, the value will be 24 for both columns
        * If no data is available, a column is 99
    * **Number of years in business** → Minimum years in business, maximum years in business
        * Type: Integer
        * YEARS_IN_BUSINESS_MIN = minimum number of years in business
        * YEARS_IN_BUSINESS_MAX = maximum number of years in business
        * 99 represents no data available or undefined upper bound
            * I.e. 5+ years is represented as min = 5, max = 99
        * Min, max = 0 means the restaurant has been open less than 30 days or is pre-open
    * **Number of employees** → Minimum number of employees, maximum number of employees
        * Type: Integer
        * NUMBER_OF_EMPLOYEES_MIN = minimum number of employees
        * NUMBER_OF_EMPLOYEES_MAX = maximum number of employees
        * 99 represents no data available or undefined upper bound
            * I.e. ‘Over 50’ is represented as min=50, max=99
    * **Average check** → Minimum check amount, maximum check amount
        * Type: Integer
        * AVERAGE_CHECK_MIN = minimum average check amount
        * AVERAGE_CHECK_MAX = maximum average check amount
        * 99 represents no data available
    * **Annual sales** → Minimum amount of revenue, maximum amount of revenue
        * Type: Integer
        * ANNUAL_SALES_MIN = minimum amount of sales per year
        * ANNUAL_SALES_MAX = maximum amount of sales per year
        * 99 represents no data available
        * 9999999 represents undefined upper bound
            * I.e. annual sales = >5,000,000 → min=5000000, max=9999999
        * If there is no annual sales data, then both ANNUAL_SALES_MIN = 99 and ANNUAL_SALES_MAX = 99


### Appendix
##### CHD menu groupings
###### Pizza
* '2.2.01. PIZZERIA'

###### Salad
* '3.0.01. HEALTHY SALADS'

###### Mexican
* '1.5.04. SOUTHWESTERN / TEX-MEX',
* '2.4.01. MEXICAN'

###### Sandwiches
* '4.0.01. SANDWICHES',
* '4.0.03. SUBS',
* '4.0.05. ROAST BEEF',
* '4.0.07. DELI',
* '4.0.99. SANDWICHES MENU UNCLASSIFIED'

###### Fast food
* '4.0.02. HAMBURGERS',
* '4.0.04. HOT DOGS',
* '4.0.06. SOUP / CHILI',
* '4.0.09. KEBABS',
* '1.2.06. CHICKEN ROTISSERIE'

###### Meat
* '1.1.06. BAR & GRILL',
* '1.2.01. FAMILY STEAK / CHOPHOUSE',
* '1.2.02. SEAFOOD & FISH',
* '1.2.03. FISH & CHIPS',
* '1.2.04. STEAK & SEAFOOD',
* '1.2.05. BARBECUE',
* '1.2.07. FRIED CHICKEN',
* '1.2.08. CHICKEN WINGS',
* '1.2.09. ETHNIC CHICKEN'

###### American
* '1.5.01. CAJUN & CREOLE',
* '1.5.02. CALIFORNIAN',
* '1.5.05. SOUTHERN & SOUL',
* '1.5.06. HAWAIIAN',
* '2.1.01. AMERICAN TRADITIONAL'

###### European
* '2.2.02. ITALIAN - PIZZA & PASTA',
* '2.2.03. FRENCH',
* '2.2.06. ARMENIAN',
* '2.2.07. IRISH, ENGLAND',
* '2.2.09. SPANISH, TAPAS',
* '2.2.10. PORTUGUESE',
* '2.2.11. GERMAN, AUSTRIAN',
* '2.2.12. SWITZERLAND, FONDUE',
* '2.2.13. EASTERN EUROPE (POLISH, HUNGARIAN...)',
* '2.2.14. RUSSIAN',
* '2.2.15. BELGIAN',
* '2.2.16. NORDIC, SCANDIC',
* '2.2.99. EUROPEAN UNCLASSIFIED'

###### Asian
* '2.3.01. CHINESE',
* '2.3.02. INDIAN / PAKISTANI / BANGLADESHI / SRI LANKAN',
* '2.3.03. JAPANESE',
* '2.3.04. SUSHI',
* '2.3.05. KOREAN',
* '2.3.06. THAI',
* '2.3.07. VIETNAMESE',
* '2.3.08. OTHER ASIAN (CAMBODIA, MALAYSIA, INDONESIA, …)',
* '2.3.99. ASIAN UNCLASSIFIED'

###### Latin American
* '2.4.02. BRAZILIAN',
* '2.4.03. OTHER LATIN AMERICA',
* '2.4.04. CARIBBEAN',
* '2.4.99. SOUTH & CENTRAL AMERICA/ CARIBBEAN UNCLASSIFIED'

###### Mediterranean
* '2.2.08. MEDITERRANEAN',
* '2.2.04. GREEK',
* '2.2.05. TURKISH',
* '2.5.02. AFRICAN',
* '2.5.03. KOSHER / JEWISH / ISRAELI',
* '2.5.04. MIDDLE EAST (EGYPTIAN, SYRIAN, IRANIAN)',
* '2.5.05. LEBANESE',
* '2.5.99. OTHER ETHNIC FOOD UNCLASSIFIED'

###### Fusion
* '2.6.01. FUSION'

###### Vegetarian
* '3.0.02. VEGETARIAN'

###### Healthy Misc
* '3.0.99. HEALTH FOOD UNCLASSIFIED'

###### Baked goods
* '5.0.01. BAKERY CAFE',
* '5.0.02. PASTRY & CAKES',
* '5.0.03. BAGELS',
* '5.0.04. PRETZELS',
* '5.0.05. DONUTS',
* '5.0.06. SNACKS',
* '5.0.99. BAKERIES / DONUTS / SNACKS UNCLASSIFIED'

###### Ice cream
* '6.0.01. ICE CREAM / DAIRY'

###### Drinks
* '7.1.01. COFFEE / TEA',
* '7.2.01. SMOOTHIE / JUICE'

###### Varied
* ‘1.1.01. VARIED MENU'


#### Filtered rows
###### Kept rows with MARKET_SEGMENT:
* '2110. FSR - CAFÉ-RESTAURANTS, FAMILY-STYLE, DINER'
* '2140. FSR - FINE DINING RESTAURANTS'
* '2120. FSR - TRADITIONAL RESTAURANTS, CASUAL DINING'
* '2130. FSR - UPSCALE CASUAL DINING RESTAURANTS'
* '2360. LSR - BUFFET RESTAURANTS'
* '2342. LSR - COFFEE SHOPS, TEA HOUSES'
* '2330. LSR - DELIVERY AND TAKE AWAY ONLY'
* '2320. LSR - FAST CASUAL'
* '2341. LSR - ICE CREAM PARLORS, FROZEN DESSERTS'
* '2343. LSR - SMOOTHIE, JUICE'
* '2310. LSR - QUICK SERVICE RESTAURANTS, FAST FOOD, SNACKS'

###### Dropped columns with MARKET_SEGMENT:
* '2429. UNCODED HOTELS & MOTELS WITHOUT RESTAURANT'
* '6120. RESTAURANT - REGIONAL / CONTINENT OFFICE'
* '6115. RESTAURANT - WORLD WIDE ULTIMATE PARENT - CHAIN'
* '6110. RESTAURANT - WORLD WIDE ULTIMATE PARENT - GROUP'
* '6130. RESTAURANT - COUNTRY HEADQUARTER OFFICE'
* '2419. UNCODED HOTELS & MOTELS WITH RESTAURANT'
* '6160. RESTAURANT - MULTI-FRANCHISE OPERATOR'
* '2424. 2* HOTELS WITHOUT RESTAURANT-MIDSCALE'
* '2412. 4* HOTELS WITH RESTAURANT-UPPER SCALE'
* '2425. 1* HOTELS WITHOUT RESTAURANT-ECONOMY'
* '2423. 3* HOTELS WITHOUT RESTAURANT-UPSCALE'
* '6170. RESTAURANT - MULTI-CONCEPT OPERATOR'
* '2414. 2* HOTELS WITH RESTAURANT-MIDSCALE'
* '2415. 1* HOTELS WITH RESTAURANT-ECONOMY'
* '2413. 3* HOTELS WITH RESTAURANT-UPSCALE'
* '2411. 5* HOTELS WITH RESTAURANT-LUXURY'
* '6150. RESTAURANT - SUB-FRANCHISEES'
* '6710. HEALTHCARE SYSTEM'
* '6715. NURSING HOME SYSTEM'
* '1245. AFFILIATED BUILDINGS'
* '3290. OTHER FOOD RETAILERS'
* '3210. RETAIL BAKERY, PASTRY'
* '3240. FISH & SEAFOOD MARKET'
* '3330. C-STORES / GAS STATIONS'
* '6950. RETAIL - SUB-FRANCHISEES'
* '1100. BUSINESS & INDUSTRY (B&I)'
* '1243. TECHNICAL AND TRADE SCHOOLS'
* '2810. NEIGHBORHOOD BARS & TAVERNS'
* '3260. FRUIT AND VEGETABLE MARKETS'
* '2830. NIGHTLIFE VENUES'
* '1241. 2 YEAR COLLEGE & UNIVERSITIES'
* '1242. 4 YEAR COLLEGE & UNIVERSITIES'
* '2662. ARENAS, STADIUMS'
* '2700. EVENT CATERING, PARTY SERVICE'
* '6810. GROUP PURCHASING ORGANIZATION'
* '1290. OTHER EDUCATION ESTABLISHMENTS'
* '2430. CAMPING, RV PARKS, CAMPGROUNDS'
* '6250. HOTEL - HOTEL MANAGEMENT FIRMS'
* '1210. PRE-K, KINDERGARTEN, CHILD CARE'
* '1390. OTHER HEALTHCARE ESTABLISHMENTS'
* '2530. AIRPORTS AND IN-FLIGHT CATERING'
* '1250. SCHOOL DISTRICTS'
* '6260. HOTEL - MULTI-FRANCHISE OPERATOR'
* '1230. SECONDARY SCHOOL'
* '2665. GOLF CLUBHOUSES'
* '1520. CENTERS FOR CHILDREN, SUMMER CAMPS'
* '1352. ASSISTED LIVING'
* '6230. HOTEL - COUNTRY HEADQUARTER OFFICE'
* '3320. GROCERY STORES'
* '2630. CASINO, GAMING'
* '1600. OTHER SEGMENTS'
* '1220. PRIMARY SCHOOL'
* '3340. LIQUOR STORES'
* '3220. CONFECTIONERS'
* '1353. SENIOR LIVING'
* '6600. DISTRIBUTORS'
* '6440. MANAGEMENT FIRM - COUNTRY SECTOR OFFICE'
* '2661. SPORTS CLUBS'
* '6210. HOTEL - WORLDWIDE ULTIMATE PARENT - GROUP'
* '6215. HOTEL - WORLDWIDE ULTIMATE PARENT - CHAIN'
* '6720. HEALTHCARE SYSTEM AND NURSING HOME SYSTEM'
* '1351. NURSING HOME'
* '6910. RETAIL - WORLDWIDE ULTIMATE PARENT - GROUP'
* '6915. RETAIL - WORLDWIDE ULTIMATE PARENT - CHAIN'
* '2610. RECREATION, LEISURE SITES, ATTRACTION PARKS'
* '3230. MEAT MARKET'
* '2820. SPORTS BARS'
* '2650. EXHIBITION / CONFERENCE CENTERS, HALL RENTAL'
* '2440. BED & BREAKFAST, GUESTHOUSES, CHAMBRES D'HOTES'
* '3380. CLUB STORES / PROFESSIONAL SHOP / CASH & CARRY'
* '1510. SOCIAL AND PRIVATE CLUBS, LEGIONS, FRATERNITIES'
* '2640. CULTURAL SITES, MUSEUMS, CINEMAS, THEATERS, ZOO'
* '2850. WINE BARS'
* '1310. HOSPITALS'
* '2620. AMUSEMENT ESTABLISHMENT, BOWLING CENTER, POOL HALL'
* '1340. CENTERS FOR REHABILITATION AND READAPTION, ADULT DAY CARE'
